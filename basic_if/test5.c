// 変数aが、{pattern1,pattern2}のどちらにおいて割り切れるのかをこたえよ

#include <stdio.h>

int main(void){
  int a;

  scanf("%d",&a);

  if(a%2==0){
    printf("pattern1\n");
  }else{
    printf("pattern2\n");
  }

  return 0;
}
